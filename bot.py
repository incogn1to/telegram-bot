#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import logging
import requests 
import time
import json
import mgrs

from datetime import datetime
from telegram.ext import Updater, CommandHandler, MessageHandler, ConversationHandler, RegexHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
 
url = 'http://142.93.226.94:8888/points'
header = {"content-type": "application/json"}
SIDE, UNIT, SIZE, ACTION, LOCATION, DETAILS = range(6)

gui_report = { 
            'alignment' :  "",
            'type' :  "",
            'size' :  "",
            'activity' : "",
            "location": [0,0],
            'timestamp' :  0,
            'additional' : "" } 

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled report", user.first_name)
    update.message.reply_text('Take care!',reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END

def help(update, context):
    update.message.reply_text('Report coordinates using report command')
    update.message.reply_text('E.g.  /report foe;mechanised infantry;company;moving NE;34VFH8024596114;1580566611;T-90;')

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def list(update, context):
    try:
        x = requests.get( url )
        code = x.status_code 
        print('code:'+ str(code)) 
        data = x.json()
        print(data)
        update.message.reply_text(data)
    except Exception as e: 
        logger.error('exception: '+ str(e))
 
def remove(update, context):
    try:
        msg = update.message.text.split(' ')
        del_url = url + "/" + msg[1]
        x = requests.delete( del_url )
        print(del_url)
        code = x.status_code 
        print('code:'+ str(code))
        print(x.text)
        if code == 204:
            update.message.reply_text('report removed')
        else:
            update.message.reply_text("can't remove")
    except Exception as e: 
        logger.error('exception: '+ str(e))
        update.message.reply_text("can't remove")

def reportLocationString(update, context):
    try:
        msg = update.message.text.split(';')
        print(msg)
        cmd = msg.pop(0)
        cmd = cmd.split(" ", 1)
        full = cmd + msg
        report = { 
            'alignment' :  full[1],
            'type' : full[2],
            'size' : full[3],
            'activity' : full[4],
            "location": convertCoordinates( full[5] ),
            'timestamp' :  int(time.time()),
            'additional' : full[7] }
        sendReport(report)
        update.message.reply_text('Report recorded')
    except Exception as e: 
        logger.error('exception: '+ str(e))
        update.message.reply_text('Say agian! Report in SALT!')

def sendReport(report):
    try:
        print(report)
        x = requests.post( url, json.dumps(report), headers = header )
        code = x.status_code 
        print('code:'+ str(code))
        print(x.text)
        if code == 200:
            print('report accepted')
        else:
            print('code:'+ str(code))
    except Exception as e: 
        logger.error('exception: '+ str(e))

def convertCoordinates(string):
    m = mgrs.MGRS()
    #d = m.toLatLon('15TWG0000049776')
    #d = m.toLatLon('35VLC15212995')
    d = m.toLatLon(str(string))
    coord = [ d[1], d[0] ]
    return coord

def start(update, context):
    gui_report.clear()
    reply_keyboard = [['foe', 'friend']]
    update.message.reply_text(
        'select side: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return SIDE

def ask_unit(update, context):
    msg = update.message.text
    gui_report[ 'alignment' ] = msg 
    print(msg)
    update.message.reply_text('Acknowledge')
    reply_keyboard = [['tank','recce','mechanised infantry']]
    update.message.reply_text(
        'select unit type: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return UNIT

def ask_size(update, context):
    msg = update.message.text
    print(msg)
    gui_report[ 'type' ] = msg 
    update.message.reply_text('Acknowledge')
    reply_keyboard = [['team','squad','platoon','company','battalion']]
    update.message.reply_text(
        'select unit size: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return SIZE

def ask_location(update, context):
    msg = update.message.text
    print(msg)
    gui_report[ 'activity' ] = msg 
    update.message.reply_text('Acknowledge')
    reply_keyboard = [['34VFH8147699250','34VFH8024596114','34VFH7637494991','34VFH7948193897']]
    update.message.reply_text(
        'report location: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return LOCATION  

def ask_action(update, context):
    msg = update.message.text
    print(msg)
    gui_report['size'] = msg 
    update.message.reply_text('Acknowledge')
    reply_keyboard = [['stationary','stationary in cover', 'moving N', 'moving E', 'moving S', 'moving W',]]
    #values stationary,stationary in cover,moving N,moving NE,moving E,moving SE,moving S,moving SW,moving W
    update.message.reply_text(
        'select action: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return ACTION

def ask_details(update, context):
    msg = update.message.text
    print(msg)
    gui_report[ 'location' ] = convertCoordinates( msg ) 
    update.message.reply_text('Acknowledge')
    reply_keyboard = [['None', 'T-92', 'T-72']]
    update.message.reply_text(
        'details: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return DETAILS

def end(update, context):
    try:
        msg = update.message.text
        print(msg)
        gui_report[ 'details' ] = msg 
        update.message.reply_text('Acknowledge')
        gui_report[ 'timestamp' ] = int(time.time())
        sendReport(gui_report)
        update.message.reply_text('Report recorded')
    except Exception as e: 
        logger.error('exception: '+ str(e))
        update.message.reply_text('Say agian! Report in SALT!')
    finally:
        return ConversationHandler.END

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("TOKEN", use_context=True)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('a', start),CommandHandler('start', start)],
        states={
            SIDE: [MessageHandler(Filters.regex('^(foe|friend)$'),ask_unit)],
            UNIT: [MessageHandler(Filters.regex('^(tank|recce|mechanised infantry)$'),ask_size)],
            SIZE: [MessageHandler(Filters.regex('^(team|squad|platoon|company|battalion)$'),ask_action)],
            ACTION: [MessageHandler(Filters.text,ask_location)],
            LOCATION: [MessageHandler(Filters.text, ask_details),CommandHandler('skip',ask_details)], 
            DETAILS: [MessageHandler(Filters.text, end),CommandHandler('skip',end)] 
        },
        fallbacks=[CommandHandler('cancel', cancel),CommandHandler('x', cancel)]
    )

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("report", reportLocationString))
    dp.add_handler(CommandHandler("ls", list))
    dp.add_handler(CommandHandler("rm", remove))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()     
